<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\User;
use App\Todo;
use Auth; 

class TodoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id = Auth::id();
        $todos = User::find($id)->todos; 
        return view('todos.index', compact('todos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user_id = Auth::id();
        return view('todos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'item' => 'required'
        ]);

        $todo = new Todo();
        $id = Auth::id();
        $todo->item = $request->item;
        $todo->user_id = $id;
        $todo->status = false;  
        $todo->save();
        return redirect('todos');       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $todo = Todo::findOrFail($id); 
        return view('todos.edit', compact('todo'));  
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //only if this todo belongs to user 
        $todo = Todo::findOrFail($id);
        if($request->ajax()){
            if($todo->user->id == Auth::id()){
                $todo->update($request->except(['_token']));
            }
            return Response::json(array('result' => 'success','status' => $request->status ), 200);    
        } else {
            $this->validate($request, [
                'item' => 'required'
            ]);            
            $todo->update($request->except(['_token']));
            return redirect('todos');            
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $todo = Todo::findOrFail($id);
        $todo->delete(); 
        return redirect('todos');
    }
}
