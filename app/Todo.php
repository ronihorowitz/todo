<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Todo extends Model
{
    
    protected $fillable = [
        'item','status','_token',
    ];    
    
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
