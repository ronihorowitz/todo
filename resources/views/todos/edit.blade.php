@extends('layouts.app')

@section('content')

{!! Form::model($todo, ['method'=>'PATCH', 'action' => ['TodoController@update', $todo->id]]) !!}
<div class = "form-group col-4" >  
{!! Form::label('item', 'What would you really like to do?') !!} 
{!! Form::text('item', null, ['class'=> 'form-control']) !!}
</div>
<div class = "form-group col-4" >
  {!! Form::submit('Do it!', ['class'=>'btn btn-primary']) !!} 
  <a href="{{route('todos.index')}}" class="btn btn-secondary" role="button" aria-pressed="true">Cancel</a> 
</div> 
{!! Form::close() !!}

@if(count($errors)>0)

<ul>
@foreach($errors->all() as $error)

    <li>{{$error}}</li>

@endforeach
</ul>

@endif 

{!! Form::model($todo, ['method'=>'DELETE', 'action' => ['TodoController@destroy', $todo->id]]) !!}
<div class = "form-group col-4" >  
  {!! Form::submit('Delete it', ['class'=>'btn btn-danger']) !!}
</div> 

{!! Form::close() !!}

@endsection