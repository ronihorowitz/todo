@extends('layouts.app')

@section('content')
    <H1>These are your tasks:</H1>
    <ul>
        @foreach($todos as $todo)
        <li>
        @if ($todo->status)
            <input type = 'checkbox' id ="{{$todo->id}}" checked>
        @else
            <input type = 'checkbox' id ="{{$todo->id}}">
        @endif
        <a href="{{route('todos.edit', $todo->id)}}">{{$todo->item}}</a> 
        </li>
        @endforeach
    </ul>
    <a href = "{{route('todos.create')}}" style ="margin-left:1em">Add a task</a>
    <script>
        $(document).ready(function(){
            $(":checkbox").click(function(event){
                console.log(event.target.checked);
                $.ajax({
                    url: "{{url('todos')}}"+"/"+event.target.id,
                    dataType: 'json',
                    type: 'put',
                    contentType: 'application/json',
                    data: JSON.stringify({ "status": event.target.checked,
                                            _token: '{{csrf_token()}}'}),
                    processData: false,
                    success: function( data, textStatus, jQxhr ){
                         console.log(JSON.stringify( data ));
                    },
                    error: function( jqXhr, textStatus, errorThrown ){
                        console.log( errorThrown );
                    }
                });                
            });
        });
    </script>    
@endsection