<?php

use Illuminate\Database\Seeder;

class TodosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('todos')->insert([
            'item' => Str::random(10),
            'user_id' => 1,
            'status' => 1,
            'created_at' => date("Y-m-d H:i:s"),
        ]);        //
    }
}
